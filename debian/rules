#!/usr/bin/make -f

# DH_VERBOSE := 1

# for hardening you might like to uncomment this:
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

DPKG_EXPORT_BUILDFLAGS := 1
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

ifeq (, $(filter nocheck, ${DEB_BUILD_OPTIONS}))
# get the current IP address status of the loopback device
ip_desc := $(shell inetutils-ifconfig --format=unix lo)
else
ip_desc := inet 127.0.0.1
endif

# reset these env, or some tests would fail due if libproxy picks them up
export https_proxy :=
export http_proxy :=
export all_proxy :=

ENABLE_LIBPROXY = --enable-libproxy
ifneq (, $(filter $(DEB_HOST_ARCH), sparc64))
ENABLE_LIBPROXY =
endif

%:
	dh $@

override_dh_auto_clean:
	if [ -f Makefile ] ; then dh_auto_clean ; fi
	if [ -d debian/tmp_save_gmo ] ; then mv debian/tmp_save_gmo/*.gmo po ; rmdir debian/tmp_save_gmo ; fi

override_dh_clean:
	dh_clean
	rm -rf build-aux
	rm -rf docs/html
	find . -name Makefile.in -delete
	if [ -d lib.bak ] ; then rm -rf lib; mv lib.bak lib ; fi

execute_before_dh_autoreconf:
	cp -a lib lib.bak
# The original upstream tarball needs bootstrap to initialize gnulib
#	./bootstrap --no-git --gnulib-srcdir=/usr/share/gnulib

override_dh_auto_configure:
	mkdir debian/tmp_save_gmo
	cp -a po/*.gmo debian/tmp_save_gmo
	dh_auto_configure -- --enable-fsanitize-ubsan=no \
	 --with-lzma --with-bzip2 \
	 --without-libhsts \
	 $(ENABLE_LIBPROXY) --enable-year2038

override_dh_auto_build:
	DEB_VERSION=$(DEB_VERSION) dh_auto_build

override_dh_auto_test:
ifeq (, $(filter nocheck, $(DEB_BUILD_OPTIONS)))
ifeq (, $(findstring inet 127.0.0.1, $(ip_desc)))
	$(warning Do not run the test suite because it fails on IPv6-only hosts.)
else
ifneq (, $(filter $(DEB_HOST_ARCH), hurd-amd64))
# some different flaky tests in different versions, annoying but still meaningful
	-dh_auto_test
else
# run tests normally
	dh_auto_test
endif
endif
endif
